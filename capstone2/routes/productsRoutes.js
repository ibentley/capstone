const express = require('express');

const productsControllers = require('../controllers/productsControllers.js');

const auth = require('../auth.js');

const router = express.Router();

// With Params
//Route for adding product in our db.
router.post('/addProduct', auth.verify, productsControllers.addProducts);

// Route for retrieving all products
router.get('/', auth.verify, productsControllers.getAllProducts);

// Route for retrieving all active products
router.get('/activeProducts', productsControllers.getActiveProducts);








// With Params
// Route for retrieving single product
router.get('/:productId', productsControllers.getProduct)

// Route for Update Product information (Admin only)
router.patch('/:productId', auth.verify, productsControllers.updateProduct)

// Route for Archive Product (Admin only)
router.patch('/:productId/archive', auth.verify, productsControllers.archiveProduct)

// Route for Activate Product (Admin only)
router.patch('/:productId/activate', auth.verify, productsControllers.activateProduct)




module.exports = router;