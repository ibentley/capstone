const express = require('express');

const mongoose = require('mongoose');

const cors = require('cors')

const usersRoutes = require('./routes/usersRoutes.js')

const productsRoutes = require('./routes/productsRoutes.js')

const port = 8888;

const app = express();

	// Mongo DB connection
	mongoose.connect("mongodb+srv://admin:admin@batch288gonzales.zfsmxfa.mongodb.net/E-commerceAPI?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    let db = mongoose.connection;

    db.on("error", console.error.bind(console, "Error! can't connect to the database"));

 	db.once("open", () => console.log("We're connected to the cloud database!"));



 	// Middlewares
 	app.use(express.json())
 	app.use(express.urlencoded({extended:true}))

 	app.use(cors());

 	app.use('/users', usersRoutes)
 	app.use('/products', productsRoutes)


 	app.listen(port, ()=> {
 			console.log(`Server is running at port ${port}!`)
 		})

