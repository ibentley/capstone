const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name:{
		type: String
	},

	description: {
		type: String
	},

	price:{
		type: Number
	},

	isActive:{
		type: Boolean,
		required: [true, "Course status is required!"]
	},

	stock: {
		type: Number
	},

	category: {
		type: String
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	userOrders: [
			{
				userId: {
					type: String
				}
				
			}

		]
})

const Products = mongoose.model("Products", productSchema);

module.exports = Products;
